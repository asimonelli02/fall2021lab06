import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {

    TextField tf1;
    TextField tf2;
    TextField tf3;
    TextField tf4;
    String choice;
    RpsGame game;

    
    public RpsChoice(TextField tf, TextField tf2, TextField tf3, TextField tf4, String choice, RpsGame game){
        this.tf1 = tf;
        this.tf2 = tf2;
        this.tf3 = tf3;
        this.tf4 = tf4;
        this.choice = choice;
        this.game = game;
    }

    public void handle(ActionEvent e){
        String round = game.playRound(choice);
        if(round.equals("It's a tie") ){
            tf1.setText(round);
            tf2.setText("Wins: " + game.getWins());
            tf3.setText("Losses: " + game.getLosses());
            tf4.setText("Ties: " + game.getTies());
        }
        else if(round.equals("Computer Wins") ){
            tf1.setText(round);
            tf2.setText("Wins: " + game.getWins());
            tf3.setText("Losses: " + game.getLosses());
            tf4.setText("Ties: " + game.getTies());
        }
        else{
            tf1.setText(round);
            tf2.setText("Wins: " + game.getWins());
            tf3.setText("Losses: " + game.getLosses());
            tf4.setText("Ties: " + game.getTies());
        }
        
    }
}
