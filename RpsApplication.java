import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


    public class RpsApplication extends Application {
        public void start (Stage stage) {
            Group root = new Group();
            HBox buttons = new HBox();
            HBox textFields = new HBox();
            VBox vbox = new VBox();
            Button rock = new Button("rock");
            Button scissors = new Button("scissors");
            Button paper = new Button("paper");
            TextField tf1 = new TextField("Welcome!");
            TextField tf2 = new TextField("Wins:");
            TextField tf3 = new TextField("Losses:");
            TextField tf4 = new TextField("Ties:");
            RpsGame game = new RpsGame(0, 0, 0);
            rock.setOnAction(new RpsChoice(tf1, tf2, tf3, tf4, "rock", game));
            scissors.setOnAction(new RpsChoice(tf1, tf2, tf3, tf4, "scissor", game));
            paper.setOnAction(new RpsChoice(tf1, tf2, tf3, tf4, "paper", game));

            tf1.setPrefWidth(200);
            tf2.setPrefWidth(200);
            tf3.setPrefWidth(200);
            tf4.setPrefWidth(200);
            buttons.getChildren().addAll(rock, scissors, paper);
            textFields.getChildren().addAll(tf1, tf2, tf3, tf4);
            vbox.getChildren().addAll(buttons, textFields);
            root.getChildren().add(vbox);

            Scene scene = new Scene(root, 650, 300);
            scene.setFill(Color.BLACK);

            stage.setTitle("Rock Paper Scissors");
            stage.setScene(scene);

            stage.show();
        }

        public static void main(String[] args) {
            Application.launch(args);
        }
    }

