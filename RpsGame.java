import java.util.Random;

public class RpsGame {
    int wins = 0;
    int ties = 0;
    int losses = 0;

    Random rand = new Random();

    public RpsGame(int wins, int ties, int losses){
        this.wins = wins;
        this.ties = ties;
        this.losses = losses;
    }

    public int getWins(){
        return this.wins;
    }

    public int getTies(){
        return this.ties;
    }

    public int getLosses(){
        return this.losses;
    }

    public String playRound(String pChoice){
        int cChoice = rand.nextInt(3);
        if(pChoice.equals("rock") && cChoice == 0){
            this.ties++;
            return "It's a tie";
        }

        else if(pChoice.equals("rock") && cChoice == 1){
            this.losses++;
            return "Computer Wins";
        }

        else if(pChoice.equals("rock") && cChoice == 2){
            this.wins++;
            return "You win";
        }

        else if(pChoice.equals("paper") && cChoice == 0){
            this.wins++;
            return "You win";
        }

        else if(pChoice.equals("paper") && cChoice == 1){
            this.ties++;
            return "It's a tie";
        }

        else if(pChoice.equals("paper") && cChoice == 2){
            this.losses++;
            return "Computer wins";
        }

        else if(pChoice.equals("scissor") && cChoice == 0){
            this.losses++;
            return "Computer wins";
        }

        else if(pChoice.equals("scissor") && cChoice == 1){
            this.wins++;
            return "You win";
        }

        else{
            this.ties++;
            return "It's a tie";
        }
    }
}